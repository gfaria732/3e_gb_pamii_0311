import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LitaImoveisPageRoutingModule } from './lita-imoveis-routing.module';

import { LitaImoveisPage } from './lita-imoveis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LitaImoveisPageRoutingModule
  ],
  declarations: [LitaImoveisPage]
})
export class LitaImoveisPageModule {}
