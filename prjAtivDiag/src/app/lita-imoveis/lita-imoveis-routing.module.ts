import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LitaImoveisPage } from './lita-imoveis.page';

const routes: Routes = [
  {
    path: '',
    component: LitaImoveisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LitaImoveisPageRoutingModule {}
